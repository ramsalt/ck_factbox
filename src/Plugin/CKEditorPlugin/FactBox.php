<?php

/**
 * @file
 * Definition of \Drupal\ck_factbox\Plugin\CKEditorPlugin\FactBox.
 */

namespace Drupal\ck_factbox\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Ramsalt Factbox" plugin.
 *
 * @CKEditorPlugin(
 *   id = "factbox",
 *   label = @Translation("Ramsalt Factbox")
 * )
 */
class FactBox extends CKEditorPluginBase
{
    /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
     */
    public function isInternal()
    {
        return FALSE;
    }

    /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
     */
    public function getFile()
    {
        $module_path = \Drupal::service('extension.list.module')->getPath('ck_factbox');
        return $module_path . '/js/plugins/factbox/plugin.js';
    }

    /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
     */
    public function getButtons()
    {
        $module_path = \Drupal::service('extension.list.module')->getPath('ck_factbox');
        return [
            'factbox' => [
                'label' => t('Factbox Buttons'),
                'image' => $module_path . '/js/plugins/factbox/icons/hidpi/factbox.png'
            ]
        ];
    }

    /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getConfig().
     */
    public function getConfig(Editor $editor)
    {
        return array();
    }

}
