CKEDITOR.dialog.add('factbox', function (editor) {
  return {
    title: 'Add a Factbox',
    minWidth: 200,
    minHeight: 100,
    contents: [
      {
        id: 'info',
        elements: [
          {
            id: 'align',
            type: 'select',
            label: 'Align',
            items: [
              [editor.lang.common.notSet, ''],
              [editor.lang.common.alignLeft, 'left'],
              [editor.lang.common.alignRight, 'right'],
              [editor.lang.common.alignCenter, 'center']
            ],
            setup: function (element) {
              this.setValue(element.data.align);
            },
            commit: function (element) {
              element.setData('align', this.getValue());
            }
          },
          {
            id: 'insertPlaceholderText',
            type: 'checkbox',
            label: 'Insert placeholder text',
            'default': false,
            commit: function (element) {
              element.setData('insertPlaceholderText', this.getValue());
            }
          }
        ]
      }
    ]
  };
});