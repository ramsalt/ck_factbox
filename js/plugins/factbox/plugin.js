(function () {
  CKEDITOR.plugins.add('factbox', {
    requires: 'widget,dialog',
    icons: 'factbox',
    hidpi: true,

    init: function (editor) {
      // Register the editing dialog.
      CKEDITOR.dialog.add('factbox', this.path + 'dialogs/factbox.js');

      editor.addContentsCss(this.path + 'css/factbox.css');

      // Register the factbox widget.
      editor.widgets.add('factbox', {
        allowedContent: 'div(!ramsalt-ckeditor-widgets-factbox,align-left,align-right,align-center);' +
          'div(!factbox-content); div(!factbox-content-inner); div(!factbox-title); h2(!factbox-title-inner)' ,

        requiredContent: 'div(ramsalt-ckeditor-widgets-factbox)',
        editables: {
          title: {
            selector: '.factbox-title-inner',
            allowedContent: 'br strong em'
          },
          content: {
            selector: '.factbox-content-inner',
            allowedContent: 'p br ul ol li strong em; a figure img figcaption[*];'
          }
        },

        template: '<div class="ramsalt-ckeditor-widgets-factbox">' +
        '<div class="factbox-inner">' +
        '<div class="factbox-title"><h2 class="factbox-title-inner"></h2></div>' +
        '<div class="factbox-content"><div class="factbox-content-inner"></div></div>' +
        '</div>' +
        '</div>',

        button: 'Create a factbox',
        dialog: 'factbox',

        upcast: function (element) {
          return element.name == 'div' && element.hasClass('ramsalt-ckeditor-widgets-factbox');
        },

        init: function () {
          if (this.element.hasClass('align-left'))
            this.setData('align', 'left');
          if (this.element.hasClass('align-right'))
            this.setData('align', 'right');
          if (this.element.hasClass('align-center'))
            this.setData('align', 'center');
        },
        data: function () {
          this.element.removeClass('align-left');
          this.element.removeClass('align-right');
          this.element.removeClass('align-center');
          if (this.data.align)
            this.element.addClass('align-' + this.data.align);

          // If the user has selected to inject a placeholder text, insert it
          if (this.data.insertPlaceholderText == true) {
            this.element.findOne('.factbox-title-inner').setHtml('Factbox Title');
            this.element.findOne('.factbox-content-inner').setHtml('Factbox Content');
          }
        }
      });

      editor.ui.addButton('factbox', {
        label: 'Factbox',
        command: 'factbox',
        toolbar: 'insert',
        icon: this.path + "icons/" + (CKEDITOR.env.hidpi ? "hidpi/" : "") + "factbox.png"
      });
    }
  });
})();
